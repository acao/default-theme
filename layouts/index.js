module.exports = {
  BlogPage: require('./BlogPage.jsx'),
  DocsPage: require('./DocsPage.jsx'),
  SectionIndex: require('./SectionIndex.jsx')
};
